# smallstarshipconfig

## **Basic Description**

My custom starship config that makes the command line look as I require configs.

I mainly use this config with the fish shell but it should work with others shells.

## **Install Custom Config**

### **Install Starship**

First follow the [installation instructions](https://starship.rs/guide/#%F0%9F%9A%80-installation) from [Starship](https://starship.rs/) official website.

https://starship.rs/guide/#%F0%9F%9A%80-installation 

***

### **Git Down the Config**

Recommend copying or forking this project into your own [git](https://git-scm.com/) system. Then you can use git clone and all of [git features](https://git-scm.com/book/en/v2). This is really useful for making changes to one config or for multiple machines/systems. 

I create a folder under .config for the starship custom configs.
```
mkdir ~/.config/starshipcustom
```

Use Git Clone to pull down your fork/copy.

Example
```
git clone URL
```
> Info - If you not making changes or do not want to use git you can just download the config code.

***

### **Remove Old Config (If it has been generated)**

Depending on how you install Starship you might have config created for you. So it worth checking and removing this file.

```
rm ~/.config/starship.toml
```

***

### **Create Soft Link / Symbolic Link or Copy File**

I would recommend creating a Soft Link/Sybolic Link rather than copying a file. As this allows you to use git features easier.

### Soft Link / Symbolic Link

To create a link edit this command to match your git/fork and your config directory.

```
ln -s file1 link1
```

Example of command

```
ln -s ~/.config/starshipcustom/starship.toml ~/.config/starship.toml
```

Or you can just copy or move the file.

```
cp ~/.config/starshipcustom/starship.toml ~/.config/starship.toml
```
```
mv ~/.config/starshipcustom/starship.toml ~/.config/starship.toml
```
